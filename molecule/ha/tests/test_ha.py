import os
import json
import testinfra.utils.ansible_runner


testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('hashicorp-vault-ha-1')


def test_ha(host):
    with host.sudo():
        vault_token = host.file("/root/vault/rootkey").content_string
        cmd = host.run("VAULT_TOKEN=" + vault_token + " vault operator raft list-peers -tls-skip-verify -format=json")
        assert cmd.rc == 0
        peers = json.loads(cmd.stdout)
        assert len(peers['data']['config']['servers']) == 2
