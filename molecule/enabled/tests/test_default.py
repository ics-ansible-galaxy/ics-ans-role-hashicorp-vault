import os
import json
import testinfra.utils.ansible_runner


testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_service(host):
    vault = host.service('vault')
    assert vault.is_enabled
    assert vault.is_running


def test_vault_api(host):
    vault_uri = "https://hashicorp-vault-enabled:8200/v1/sys/seal-status"
    cmd = host.run('curl -k -f ' + vault_uri)
    assert cmd.rc == 0
    status = json.loads(cmd.stdout)
    assert status['initialized']
    assert not status['sealed']
    assert status['storage_type'] == 'file'


def test_vault_cli(host):
    cmd = host.run("vault status -address=https://hashicorp-vault-enabled:8200 -ca-cert=/etc/ssl/certs/hashicorp-vault-enabled-chained.crt -format=json")
    assert cmd.rc == 0
    status = json.loads(cmd.stdout)
    assert status['initialized']
    assert not status['sealed']
    assert status['storage_type'] == 'file'


def test_policy(host):
    with host.sudo():
        vault_token = host.file("/root/vault/rootkey").content.decode()
        cmd = host.run("VAULT_TOKEN=" + vault_token + " vault policy list -address=https://hashicorp-vault-enabled:8200 -ca-cert=/etc/ssl/certs/hashicorp-vault-enabled-chained.crt -format=json".encode().decode())
        assert cmd.rc == 0
        assert "foopolicy" in json.loads(cmd.stdout)


def test_ldap(host):
    with host.sudo():
        vault_token = host.file("/root/vault/rootkey").content.decode()
        cmd = host.run("VAULT_TOKEN=" + vault_token + " vault read -address=https://hashicorp-vault-enabled:8200 -ca-cert=/etc/ssl/certs/hashicorp-vault-enabled-chained.crt -format=json auth/ldap/config".encode().decode())
        assert cmd.rc == 0
        ldap_config = json.loads(cmd.stdout)
        assert ldap_config['data']['url'] == 'ldap://ldap.example.com'


def test_ldap_group_policies(host):
    with host.sudo():
        vault_token = host.file("/root/vault/rootkey").content.decode()
        cmd = host.run("VAULT_TOKEN=" + vault_token + " vault list -address=https://hashicorp-vault-enabled:8200 -ca-cert=/etc/ssl/certs/hashicorp-vault-enabled-chained.crt -format=json auth/ldap/groups".encode().decode())
        assert cmd.rc == 0
        ldap_groups = json.loads(cmd.stdout)
        assert ldap_groups == ['foogroup']
        cmd = host.run("VAULT_TOKEN=" + vault_token + " vault read -address=https://hashicorp-vault-enabled:8200 -ca-cert=/etc/ssl/certs/hashicorp-vault-enabled-chained.crt -format=json auth/ldap/groups/foogroup".encode().decode())
        assert cmd.rc == 0
        ldap_foogroup = json.loads(cmd.stdout)
        assert ldap_foogroup['data']['policies'] == ['foopolicy']
