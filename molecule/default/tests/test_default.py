import os
import json
import testinfra.utils.ansible_runner


testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_service(host):
    vault = host.service('vault')
    assert vault.is_enabled
    assert vault.is_running


def test_vault_api(host):
    vault_uri = "https://hashicorp-vault-default:8200/v1/sys/seal-status"
    cmd = host.run('curl -k -f ' + vault_uri)
    assert cmd.rc == 0
    status = json.loads(cmd.stdout)
    assert not status['initialized']
    assert status['sealed']
    assert status['storage_type'] == 'file'


def test_vault_cli(host):
    cmd = host.run("vault status -address=https://hashicorp-vault-default:8200 -ca-cert=/etc/ssl/certs/hashicorp-vault-default-chained.crt -format=json")
    assert cmd.rc == 2  # Sealed status
    status = json.loads(cmd.stdout)
    assert not status['initialized']
    assert status['sealed']
    assert status['storage_type'] == 'file'
