# ics-ans-role-hashicorp-vault

Ansible role to install [HashiCorp Vault](https://www.vaultproject.io).

## Role Variables

See: [Default variables](defaults/main.yml)

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-hashicorp-vault
```

## License

BSD 2-clause
